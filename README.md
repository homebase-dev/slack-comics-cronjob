# slack-comics-cronjob

Send comicstrips to your slack channel using some bash scripts which fetch the comic-image-url from the website and send them via slack web api. The scripts are called via cronjob, so that fresh comics are provided to you on a daily base.

Following comics are fetched at the moment: 
* [dilbert](https://dilbert.com/)
* [strangeplanet](https://www.nathanwpyle.art/)
* [smbc](https://www.smbc-comics.com/)
* [oglaf](https://www.oglaf.com)
* [extrafabulouscomics](https://extrafabulouscomics.com/)

### Setup

1) Create New Slack app [https://api.slack.com/apps](https://api.slack.com/apps)
2) Activate `Incoming Webhooks` for your slack app and copy the `hooks.slack.com...` url from the example curl request
3) Edit `comics_scripts/sendslack-notification` and insert the urlpart which you copied in the previous step (url looks something like this: `https://hooks.slack.com/services/XXXXXXXXXXX/XXXXXXXXXXX/XXXXXXXXXXXXXXXXXXXXXXXX`)
3) Execute `install` script
